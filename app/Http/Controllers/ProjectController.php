<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Project;

class ProjectController extends Controller
{
    public function index(){
        $projects = Project::select('id','project_name AS title','status','color')->get();
        // foreach ($projects as $key => $value) {
        //     $exa = Project::find($value->id);
        //     $exa->color = "8px solid rgb(".rand(0,255).",".rand(0,255).",".rand(0,255).")";
        //     $exa->save();
        // }
        return response()->json($projects);
    }
}
