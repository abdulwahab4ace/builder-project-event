<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Project;
use App\Dependent;
use App\CalendarEvent;
use App\EventDependency;

class CalendarEventController extends Controller
{
    /**
     * Get All Events
     */
    public function index(){
        $events = CalendarEvent::select('id','title','start','end','duration','proj_id AS projectId','status','classNames','backgroundColor','borderColor','textColor','predecessor AS masterIds')->get();
        return response()->json($events);
    }

    /**
     * Store new Event
     */
    public function store(Request $request){
        try {
            $event = new CalendarEvent;
            $event->title = $request->title;
            $event->start = $request->start;
            $event->end = $request->end;
            $event->duration = $request->duration;
            $event->proj_id = $request->projectId;
            $event->predecessor = $request->masterIds;
            $event->backgroundColor = substr( Project::find($request->projectId)->color,10);
            $event->borderColor = substr( Project::find($request->projectId)->color,10);
            $event->textColor = 'white';
        
            if($event->save()){
                $event->projectId = $event->proj_id;
                $event->masterIds = $event->predecessor;
                unset($event->predecessor);
                unset($event->proj_id);
                unset($event->created_at);
                unset($event->updated_at);
                return response()->json($event);
            }else{
                return response()->json(null);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage() ]);
        }
    }

    public function add_dependency(Request $request){
        try {
            $new_dependencies = $this->db_add_dependency($request->all());
            return response()->json($new_dependencies);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /** DEPRECATED */
    public function add_update_delete_dependency(Request $request){
        try {
            $dependencies = $this->db_add_update_delete_dependency($request->all());
            return response()->json($dependencies);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }
    
    private function db_add_dependency($data) {
        $master_ids = array();
        $master_ids = array_map(function($dependency){
            $dependent = Dependent::Create($dependency);
            return $dependent->master_id;
        }, $data);
        return $this->get_selected_dependencies($master_ids);
    }

    /** DEPRECATED */
    private function db_add_update_delete_dependency($data) {
        $child_id = $data['child_id'];
        $master_ids = array();
        $new_dependency = array();
        $old_dependency = Dependent::where('child_id', $child_id)->get();

        /** Update or Create selected dependeny */
        $new_dependency = array_map(function($dependency){
            $dependent = Dependent::updateOrCreate(
                ['master_id' => $dependency['master_id'], 'child_id' => $dependency['child_id']],
                ['workDaysDiff' => $dependency['workDaysDiff']]
            );
            return $dependent;
        }, $data['dependencies']);
        $new_dependency = collect($new_dependency);
        
        $ids = $this->get_unselected_dependency_ids($old_dependency->pluck('id'), $new_dependency->pluck('id'));
        /** Delete depenceis which are unselected during edit */
        $this->delete_selected_dependencies($ids);
        
        $master_ids = $this->merge_ids($new_dependency->pluck('master_id'), $old_dependency->pluck('master_id'));
        return $this->get_selected_dependencies($master_ids->toArray());
    }

    public function get_dependencies(){
        try {
            $dependencies = CalendarEvent::select('id as master')->with('child:id,master_id,child_id as id,workDaysDiff')->get();   
            $dependencies = $dependencies->reject(function($dependency, $key){
                return $dependency->child->isEmpty();
            })->values()->map(function($dependency,$key){
                $dependency->child = $dependency->child->map(function($child,$key){
                    unset($child->master_id);
                    return $child;
                });
                return $dependency;
            });
            return response()->json($dependencies);
        } catch (Exception $e) {
            return response()->json(false);
        }
    }

    public function get_selected_dependencies(Array $whereIds){
        try {
            $new = CalendarEvent::whereIn('id', $whereIds)->select('id as master')->with('child:id,master_id,child_id as id,workDaysDiff')->get();   
            $new = $new->reject(function($dependency, $key){
                return $dependency->child->isEmpty();
            })->values()->map(function($dependency,$key){
                $dependency->child = $dependency->child->map(function($child,$key){
                    unset($child->master_id);
                    return $child;
                });
                return $dependency;
            });
            return $new;
        } catch (Exception $e) {
            return false;
        }
    }

    
    /** DEPRECATED */
    private function delete_selected_dependencies($ids_collection){
        $deleted = Dependent::destroy($ids_collection);
    }

    public function update_events(Request $request){
        try {
            DB::beginTransaction();
            $events = array();

            $this->db_update_dependencies($request->dependenciesUpdate);
            $this->db_delete_dependencies($request->deletedDependency);

            foreach ($request->events as $key => $value) {
                
                $event = CalendarEvent::find($value['id']);
                $event->title = $value['title'];
                $event->start = $value['start'];
                $event->end = $value['end'];
                $event->duration = $value['duration'];
                $event->proj_id = $value['projectId'];
                $event->predecessor = $value['masterIds'];

                $color = Project::find($event->proj_id)->color;
                
                $event->backgroundColor = substr($color, 10);
                $event->borderColor = substr($color, 10);
                $event->save();

                $event->projectId = $event->proj_id;
                $event->masterIds = $event->predecessor;
                unset($event->predecessor);
                unset($event->proj_id);
                unset($event->created_at);
                unset($event->updated_at);
                unset($event->deleted_at);
                unset($event->coworker);
                unset($event->description);
                unset($event->phase);
                unset($event->status);
                unset($event->subcontractor);
                array_push($events, $event);
            }
            DB::commit();
            return response()->json($events);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(null);
        }
    }

    private function db_update_dependencies($data){
        if(!$data) { return; }
        $new_dependency = array_map(function($dependency){
            $dependent = Dependent::updateOrCreate(
                ['master_id' => $dependency['master_id'], 'child_id' => $dependency['child_id']],
                ['workDaysDiff' => $dependency['workDaysDiff']]
            );
            return $dependent;
        }, $data);
    }

    private function db_delete_dependencies($deletedDependency){
        if($deletedDependency) {
            foreach ($deletedDependency['masterIds'] as $key => $masterId) {
                Dependent::where('child_id', $deletedDependency['childId'])->where('master_id', $masterId)->delete();
            }
        }
    }

    /** DEPRECATED */
    private function get_unselected_dependency_ids($collection_1, $collection_2) {
        return $collection_1->diff($collection_2);
    }

    /** DEPRECATED */
    private function merge_ids($collection_1, $collection_2) {
        return $collection_1->merge($collection_2)->unique()->values();
    }

    public function testing(){
        echo "All Set";
    }
}
