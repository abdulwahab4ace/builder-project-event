<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalendarEvent extends Model
{
    protected $table = 'site_2_calendarevent';

    /** Array & JSON Casting */
    protected $casts = [
        'predecessor' => 'array',
        'masterIds' => 'array',
    ];

    public function child(){
        return $this->hasMany('App\Dependent','master_id', 'master');
    }
}
