<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventDependency extends Model
{
    //
    public $timestamps = false;

    public function scopeLikeJson($query, $id)
    {
        return $query->where('child','like',"%\"id\":".$id."%");
    }
}
