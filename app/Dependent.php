<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dependent extends Model
{

    public $timestamps = false;

    protected $fillable = ['master_id', 'child_id', 'workDaysDiff'];

    public function master() {
        return $this->belongsTo('App\CalendarEvent','master_id', 'master');
    }
}
