<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('projects', 'ProjectController@index');
Route::get('events', 'CalendarEventController@index');
Route::post('add_event', 'CalendarEventController@store');
Route::put('update_event', 'CalendarEventController@update');
Route::put('update_child_events', 'CalendarEventController@update_events');

Route::post('add_dependency', 'CalendarEventController@add_dependency');
Route::post('add_update_delete_dependency', 'CalendarEventController@add_update_delete_dependency');
Route::get('get_dependencies', 'CalendarEventController@get_dependencies');
Route::put('append_dependency', 'CalendarEventController@append_dependency');

Route::get('testing', 'CalendarEventController@testing');
